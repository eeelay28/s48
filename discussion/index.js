// alert('Hello World');

// mock db
let posts=[];

// post ID
let count=1;

// add post

// this will trigger an event that will add new post in our mock database upon clicking the "create " button


document.querySelector("#form-add-post").addEventListener("submit",(e)=>{

  e.preventDefault()

  posts.push({
    id:count,
    title:document.querySelector("#txt-title").value,
    body: document.querySelector("#txt-body").value
  })

  // count will increment everytime a new posts is added.

  count++
  console.log(posts)
  alert("successfully added");
  showPosts();

})

const showPosts = () => {

  // create a variable that will contain all the post.

  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `
    
      <div id = "${post.id}">
        <h2 id="post-title-${post.id}">${post.title}</h2>
        <p id="post-body-${post.id}">${post.body}</p>

        <button onClick="editPost('${post.id}')">Edit</button>
        <button onClick="deletePost('${post.id}')">Delete</button>
       

      
      </div>
    
    `
    
  })

  console.log(postEntries);

  document.querySelector("#div-post-entries").innerHTML = postEntries

}


// edit post button

// we will create a function that will be called in the onClick event and will pass the value in the update form input box

const editPost = (id) => {

  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  // pass the id, title and body from the post to be updated in the Edit Post Form
  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;

}

// update post -eventListener

document.querySelector("#form-edit-post").addEventListener("submit",(e) =>{


  e.preventDefault();

  for(let i = 0; i < posts.length; i++){

    if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
      posts[i].title = document.querySelector("#txt-edit-title").value;
      posts[i].body = document.querySelector("#txt-edit-body").value;

      showPosts(posts)
      alert("update successful")
      break;
    }
  }
})

// delete post button

// we will create a function that will be called in the onClick event and will pass the value in the update form input box

const deletePost = (id) => {

  
  posts.splice(posts.indexOf(id),1);
  
  
  const postElement = document.getElementById(`${id}`);
  postElement.remove();


  showPosts(posts)
  alert("deletion successful")

}
